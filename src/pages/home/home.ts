import { Component } from '@angular/core';
import { NavController,NavParams } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

import { LoginPage } from '../login/login';
import { AgendaPage } from '../agenda/agenda';
import { InfoPage } from '../info/info';
import { SponsorPage } from '../sponsor/sponsor';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  username: string;
  password: string;

  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iab: InAppBrowser,
  ){

  }

  register() {
    const url = 'https://thailandfoodinnovation.com/registration/form'; 
    // return false;
    let target = "_blank";
    this.iab.create(url,target,this.options);
    
  }
  floorplan() {
    const url = 'https://thailandfoodinnovation.com/exhibition/'; 
    // return false;
    let target = "_blank";
    this.iab.create(url,target,this.options);
    
  }
  info() {
    this.navCtrl.push(InfoPage);
  }

  agenda() {
    this.navCtrl.push(AgendaPage);
  }

  sponsor() {
    this.navCtrl.push('SponsorPage');
  }


  signin() {
    this.navCtrl.push(LoginPage);
  }

  // public openWithSystemBrowser(url : string){
  //   let target = "_system";
  //   this.iab.create(url,target,this.options);
  // }
  // public openWithInAppBrowser(url : string){
  //     let target = "_blank";
  //     this.iab.create(url,target,this.options);
  // }
  // public openWithCordovaBrowser(url : string){
  //     let target = "_self";
  //     this.iab.create(url,target,this.options);
  // }  

}
