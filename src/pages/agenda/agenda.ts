import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';

/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-agenda',
  templateUrl: 'agenda.html',
})
export class AgendaPage {
  // public date = moment().format('YYYY-MM-DD');
  date: number;
  segment = '1';

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  check() {
    console.log(this.date);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AgendaPage');
  }

}
