webpackJsonp([0],{

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SponsorPageModule", function() { return SponsorPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sponsor__ = __webpack_require__(276);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SponsorPageModule = /** @class */ (function () {
    function SponsorPageModule() {
    }
    SponsorPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sponsor__["a" /* SponsorPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sponsor__["a" /* SponsorPage */]),
            ],
        })
    ], SponsorPageModule);
    return SponsorPageModule;
}());

//# sourceMappingURL=sponsor.module.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SponsorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the SponsorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SponsorPage = /** @class */ (function () {
    function SponsorPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SponsorPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SponsorPage');
    };
    SponsorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sponsor',template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/pages/sponsor/sponsor.html"*/'<ion-header>\n    <ion-navbar>\n      <ion-title>Sponsor</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  <ion-content>\n    <ion-list>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/scg.jpg">\n        </ion-thumbnail>\n        <h2>Siam Cement Group</h2>\n        <!-- <p>Hayao Miyazaki • 1988</p> -->\n        <!-- <button ion-button clear item-end>View</button> -->\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/thaibev.jpg">\n        </ion-thumbnail>\n        <h2>ThaiBev</h2>\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/true.jpg">\n        </ion-thumbnail>\n        <h2>True Corporation</h2>\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/mitr-phol.jpg">\n        </ion-thumbnail>\n        <h2>Mitr Phol</h2>\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/cp-group.jpg">\n        </ion-thumbnail>\n        <h2>Charoen Pokphand Group</h2>\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/mama.jpg">\n        </ion-thumbnail>\n        <h2>Mama</h2>\n      </ion-item>\n  \n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/kbank.png">\n        </ion-thumbnail>\n        <h2>Kasikorn bank</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/bank.jpg">\n        </ion-thumbnail>\n        <h2>Bangkok Bank</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/singha.jpg">\n        </ion-thumbnail>\n        <h2>Singha Corporation</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/central.jpg">\n        </ion-thumbnail>\n        <h2>Central Group</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/cpall.jpg">\n        </ion-thumbnail>\n        <h2>CP All</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/lotus.jpg">\n        </ion-thumbnail>\n        <h2>Tesco Lotus</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/scb.jpg">\n        </ion-thumbnail>\n        <h2>Siam Commercial Bank</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/seavalue.jpg">\n        </ion-thumbnail>\n        <h2>Sea Value Group</h2>\n      </ion-item>\n      <ion-item>\n        <ion-thumbnail item-start>\n          <img src="assets/imgs/cpf.jpg">\n        </ion-thumbnail>\n        <h2>Charoen Pokphand Foods</h2>\n      </ion-item>\n  \n    </ion-list>\n  </ion-content>'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/pages/sponsor/sponsor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], SponsorPage);
    return SponsorPage;
}());

//# sourceMappingURL=sponsor.js.map

/***/ })

});
//# sourceMappingURL=0.js.map