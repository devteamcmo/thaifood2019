webpackJsonp([4],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AgendaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the AgendaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AgendaPage = /** @class */ (function () {
    function AgendaPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.segment = '1';
    }
    AgendaPage.prototype.check = function () {
        console.log(this.date);
    };
    AgendaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AgendaPage');
    };
    AgendaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-agenda',template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/pages/agenda/agenda.html"*/'\n<ion-header>\n  <ion-navbar>\n    <ion-title>Seminar</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-segment [(ngModel)]="segment" color="primary">\n    <ion-segment-button value="1">\n      21 May\n    </ion-segment-button>\n    <ion-segment-button value="2" >\n      22 May\n    </ion-segment-button>\n  </ion-segment>\n\n  <ng-container *ngIf="segment == 1">\n    <ion-list>\n      <!-- <ion-list-header>\n      </ion-list-header> -->\n\n      <ion-list-header>\n        <h2 style="color: black;">Morning</h2>\n      </ion-list-header>\n      <ion-item>\n        <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n        <div class="row">\n          <h2 class="header-item">Register for the event</h2>\n        </div>\n        <div class="row" style="color: #757575; ">\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n            08:00 - 09:00\n          </ion-col>\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n            Balloon room\n          </ion-col>\n        </div>\n          \n      </ion-item>\n\n      <ion-item>\n        <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n        <div class="row">\n          <h2 class="header-item">VTR Presentation</h2>\n        </div>\n        <div class="row" style="color: #757575; ">\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n            09:00 - 09:10\n          </ion-col>\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n            Balloon room\n          </ion-col>\n        </div>\n          \n      </ion-item>\n\n        <ion-item>\n          <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n          <div class="row">\n            <h2 class="header-item">กล่าวเปิดงาน Food for Future</h2>\n          </div>\n          <div class="row" style="color: #757575; ">\n            <ion-col col-6>\n              <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n              09:10 - 10.00\n            </ion-col>\n            <ion-col col-6>\n              <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n              Balloon room\n            </ion-col>\n          </div>\n            \n        </ion-item>\n\n        <ion-item>\n            <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n            <div class="row">\n              <h2 class="header-item">บรรยายเรื่อง “10 Technologies to Watch”</h2>\n            </div>\n            <div class="row" style="color: #757575; ">\n              <ion-col col-6>\n                <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n                10:00 - 11:00\n              </ion-col>\n              <ion-col col-6>\n                <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n                Balloon room\n              </ion-col>\n            </div>\n              \n          </ion-item>\n\n          <ion-item>\n            <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n            <div class="row">\n              <h2 class="header-item">บรรยายเรื่อง Future Trend for Food Industry</h2>\n            </div>\n            <div class="row" style="color: #757575; ">\n              <ion-col col-6>\n                <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n                11:00 - 12:00\n              </ion-col>\n              <ion-col col-6>\n                <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n                Balloon room\n              </ion-col>\n            </div>\n              \n          </ion-item>\n    </ion-list>\n\n    <ion-list>\n      <ion-list-header>\n        <h2>Afternoon</h2>\n      </ion-list-header>\n\n      <ion-item>\n        <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n        <div class="row">\n          <h2 class="header-item">Register for the event</h2>\n        </div>\n        <div class="row" style="color: #757575; ">\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n            08:00 - 09:00\n          </ion-col>\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n            Balloon room\n          </ion-col>\n        </div>\n          \n      </ion-item>\n    </ion-list>\n  </ng-container>\n\n  <ng-container *ngIf="segment == 2">\n\n    <ion-list>\n      <ion-list-header>\n        <h2>Afternoon</h2>\n      </ion-list-header>\n\n      <ion-item>\n        <!-- <ion-note item-end="" class="note note-ios"></ion-note> -->\n        <div class="row">\n          <h2 class="header-item">Exercise</h2>\n        </div>\n        <div class="row" style="color: #757575; ">\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/clock_med_gray.png"> \n            13:00 - 14:00\n          </ion-col>\n          <ion-col col-6>\n            <img style="height: 20px;"  src="assets/imgs/location_gray.png"> \n            Balloon room\n          </ion-col>\n        </div>\n          \n      </ion-item>\n    </ion-list>\n  </ng-container>\n\n\n</ion-content>'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/pages/agenda/agenda.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], AgendaPage);
    return AgendaPage;
}());

//# sourceMappingURL=agenda.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/pages/info/info.html"*/'<!--\n  Generated template for the InfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Info</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content class="bg2" padding style="background-color: #385E91; color: white;">\n    <img style="width: 100%; margin: 0;" class="img-logo" src="assets/imgs/header_info.jpg"> \n\n    <p>INNOVATIVE SOLUTIONS FOR SUSTAINABLE FOOD INDUSTRY</p>\n    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ทุกมิติของนวัตกรรม\n    เพื่ออุตสาหกรรมอาหารยั่งยืน\n    การสร้างมูลค่าให้กับสินค้าด้วยเศรษฐกิจสร้างสรรค์ เป็นแนวคิดสำคัญในการขับเคลื่อนเศรษฐกิจประเทศ\n    หอการค้าไทยและสภาหอการค้าแห่งประเทศไทย เห็นถึงศักยภาพในการยกระดับของผู้ประกอบการไทย\n    โดยการสร้างมูลค่าด้วยนวัตกรรมดังนั้นจึงได้จัดงาน Thailand Food Innovation Forum 2018\n    ในระหว่างวันที่ 21-22 พฤษภาคม 2561 ณ ศูนย์การประชุมแห่งชาติสิริกิติ์\n    ภายในงานจะมีการจัดแสดงผลงานนวัตกรรมและงานออกแบบ และกิจกรรมที่น่าสนใจมากมาย\n    ซึ่งจะเป็นเวทีให้กับผู้ประกอบการของไทย ได้นำไปปรับใช้ในเชิงธุรกิจอย่างเป็นรูปธรรมและยั่งยืน\n</ion-content>\n'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/pages/info/info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding class="bg2">\n      <ion-item class="login-item">\n        <ion-label floating>Username</ion-label>\n        <ion-input type="text"></ion-input>\n      </ion-item>\n    \n      <ion-item class="login-item">\n        <ion-label floating>Password</ion-label>\n        <ion-input type="password"></ion-input>\n      </ion-item>\n      \n      <button class="button-login" ion-button full>Sign in</button>\n\n</ion-content>\n'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 112:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 112;

/***/ }),

/***/ 153:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/agenda/agenda.module": [
		272,
		3
	],
	"../pages/info/info.module": [
		273,
		2
	],
	"../pages/login/login.module": [
		274,
		1
	],
	"../pages/sponsor/sponsor.module": [
		275,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 153;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__agenda_agenda__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__info_info__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, navParams, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.iab = iab;
        this.options = {
            location: 'yes',
            hidden: 'no',
            clearcache: 'yes',
            clearsessioncache: 'yes',
            zoom: 'yes',
            hardwareback: 'yes',
            mediaPlaybackRequiresUserAction: 'no',
            shouldPauseOnSuspend: 'no',
            closebuttoncaption: 'Close',
            disallowoverscroll: 'no',
            toolbar: 'yes',
            enableViewportScale: 'no',
            allowInlineMediaPlayback: 'no',
            presentationstyle: 'pagesheet',
            fullscreen: 'yes',
        };
    }
    HomePage.prototype.register = function () {
        var url = 'https://thailandfoodinnovation.com/registration/form';
        // return false;
        var target = "_blank";
        this.iab.create(url, target, this.options);
    };
    HomePage.prototype.floorplan = function () {
        var url = 'https://thailandfoodinnovation.com/exhibition/';
        // return false;
        var target = "_blank";
        this.iab.create(url, target, this.options);
    };
    HomePage.prototype.info = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__info_info__["a" /* InfoPage */]);
    };
    HomePage.prototype.agenda = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__agenda_agenda__["a" /* AgendaPage */]);
    };
    HomePage.prototype.sponsor = function () {
        this.navCtrl.push('SponsorPage');
    };
    HomePage.prototype.signin = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/pages/home/home.html"*/'<!-- <ion-header class="home-navbar">\n    <ion-title style="text-align: center;">\n        <img  class="img-logo" src="assets/imgs/banner2019.png"> \n    </ion-title>\n</ion-header> -->\n\n<ion-content no-bounce padding class="bg-content" >\n    <ion-title style="text-align: center;">\n        <img  class="img-logo" src="assets/imgs/banner2019.png"> \n    </ion-title>\n  <ion-slides pager>\n\n    <ion-slide>\n      <!-- <h2>Slide 1</h2> -->\n      \n      <ion-row class="row">\n        <ion-col class="col-sm-3">\n          <button (click)="register()" icon-start="" ion-button="" class="button-default button-home">\n            <div>\n              <div>\n                <img style="height: 40px;"  src="assets/imgs/regis2.png"> \n              </div>  \n              <label class="text-btn">Register</label>\n            </div>\n          </button>\n          \n          <!-- <button style="" color="danger" icon-only="" ion-button="" outline="" class="disable-hover button button-ios button-outline button-outline-ios button-outline-ios-danger"><span class="button-inner">\n            <ion-icon is-active="false" name="remove-circle" role="img" class="icon icon-ios ion-ios-remove-circle" aria-label="remove circle"></ion-icon>\n          </span><div class="button-effect"></div>\n          </button> -->\n        </ion-col>\n        <ion-col class="col-sm-3">             \n          <button (click)="info()" icon-start="" ion-button="" class="button-default button-home">\n              <div>\n                <div>\n                  <img style="height: 40px;"  src="assets/imgs/info.png"> \n                </div>  \n                <label class="text-btn">Info</label>\n              </div>\n          </button>\n        </ion-col>\n        <ion-col class="col-sm-3">\n          <button (click)="agenda()"  icon-start="" ion-button="" class="button-default button-home">\n              <div>\n                <div>\n                  <img style="height: 40px;"  src="assets/imgs/agenda2.png"> \n                </div>  \n                <label class="text-btn">Agenda</label>\n              </div>\n          </button>\n        </ion-col>\n      </ion-row>\n\n\n      <ion-row class="row">\n        <ion-col class="col-sm-3">\n            <button (click)="sponsor()" icon-start="" ion-button="" class="button-default button-home">\n                <div>\n                  <div>\n                  <img style="height: 40px;"  src="assets/imgs/sponsor.png"> \n                  </div>  \n                  <label class="text-btn">Sponsor</label>\n                </div>\n            </button>\n        </ion-col>\n        <ion-col class="col-sm-3">\n                <button (click)="floorplan()" icon-start="" ion-button="" class="button-default button-home">\n                    <div>\n                      <div>\n                      <img style="height: 40px;"  src="assets/imgs/map.png"> \n                      </div>  \n                      <label class="text-btn">Floorplan</label>\n                    </div>\n                </button>\n        </ion-col>\n        <ion-col class="col-sm-3">\n            <!-- <button icon-start="" ion-button="" class="disable-hover button button-ios button-default button-default-ios button-home"><span class="button-inner">\n                <ion-icon name="home" role="img" class="icon icon-ios ion-ios-home" aria-label="home"></ion-icon>\n                Blank\n              </span><div class="button-effect"></div>\n            </button> -->\n        </ion-col>\n      </ion-row>\n    </ion-slide>\n\n\n\n    <!-- <ion-slide>\n      <h2>Slide 2</h2>\n    </ion-slide> -->\n\n  </ion-slides>\n  \n</ion-content>\n\n<ion-footer >\n    <!-- <button ion-button  (click)="signin()" class="button-signin">Sign in</button> -->\n\n</ion-footer>'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser__["a" /* InAppBrowser */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(221);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(271);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_login_login__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_agenda_agenda__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_info_info__ = __webpack_require__(101);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_agenda_agenda__["a" /* AgendaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_info_info__["a" /* InfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/agenda/agenda.module#AgendaPageModule', name: 'AgendaPage', segment: 'agenda', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sponsor/sponsor.module#SponsorPageModule', name: 'SponsorPage', segment: 'sponsor', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_agenda_agenda__["a" /* AgendaPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_info_info__["a" /* InfoPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser__["a" /* InAppBrowser */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 271:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(197);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/IT/Desktop/thaifood-to-store/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/IT/Desktop/thaifood-to-store/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[198]);
//# sourceMappingURL=main.js.map